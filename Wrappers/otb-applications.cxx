// =========================================================================
//
//   Program:   otb-guile
//
//   Copyright (c) Jordi Inglada. All rights reserved.
//
//   See copyright.txt for details.
//
//   This software is distributed WITHOUT ANY WARRANTY; without even
//   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//   PURPOSE.  See the above copyright notices for more information.
//
// =========================================================================


#include <math.h>
#include "otbWrapperApplicationRegistry.h"
#include "otbWrapperApplication.h"
#include "otbWrapperTypes.h"
#include <libguile.h>

extern "C" {

  /** Utility function to convert a std::vector<std::string> into
      an SCM list*/
  SCM vec_of_strings_to_list (std::vector<std::string> vos)
  {
    scm_t_array_handle handle;
    size_t len = vos.size();
    ssize_t inc;
    SCM* elt;

    SCM vec = scm_make_vector(scm_from_unsigned_integer(len), scm_from_utf8_string(""));

    elt = scm_vector_writable_elements (vec, &handle, &len, &inc);
    for (size_t i = 0; i < len; i++, elt += inc)
      {
      *elt = scm_from_utf8_string(vos[i].c_str());
      }
    scm_array_handle_release (&handle);

    return scm_vector_to_list(vec);
  }

  /** Utility function to convert an SCM list into a std::vector<std::string>*/
  std::vector<std::string> list_to_vec_of_strings (SCM lst)
  {

    std::vector<std::string> vos;
    unsigned int list_length = scm_to_uint(scm_length(lst));
      
    for(unsigned int i = 0; i < list_length; ++i)
      {
      vos.push_back(std::string( scm_to_utf8_string(scm_list_ref(lst, scm_from_unsigned_integer(i)))) );
      }

    return vos;
  }


  /** Definition of the type for an otb_application */
  static scm_t_bits otb_application_tag;

  struct otb_application {
    otb::Wrapper::Application::Pointer application;

    /* The name of this otb_application */
    SCM name;

  };

  /** Create an otb_application smob */
  static SCM
  make_otb_application (SCM name)
  {
    SCM smob;
    struct otb_application *otb_application;

    /* Step 1: Allocate the memory block.
        */
    otb_application = (struct otb_application *)
      scm_gc_malloc (sizeof (struct otb_application), "otb_application");

    /* Step 2: Initialize it with straight code.
        */
    otb_application->name = SCM_BOOL_F;

    /* Step 3: Create the smob.
        */
    SCM_NEWSMOB (smob, otb_application_tag, otb_application);

    /* Step 4: Finish the initialization.
        */
    otb_application->name = name;

    otb::Wrapper::Application::Pointer appli =
      otb::Wrapper::ApplicationRegistry::CreateApplication(scm_to_utf8_string(name));

    
    if( appli )
      {
      otb_application->application = appli;
      return smob;
      }
    else
      {
      scm_throw(scm_from_utf8_symbol("otb-application-exception"), scm_from_utf8_symbol(scm_to_utf8_string(name)));
      }


  }

  SCM
  clear_otb_application (SCM otb_application_smob)
  {
    struct otb_application *otb_application;

    scm_assert_smob_type (otb_application_tag, otb_application_smob);

    otb_application = (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);


    otb_application->application->UnRegister();
    
    scm_remember_upto_here_1 (otb_application_smob);

    return SCM_UNSPECIFIED;
  }

  static SCM
  mark_otb_application (SCM otb_application_smob)
  {
    /* Mark the otb_application's data.  */
    struct otb_application *otb_application = (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    scm_gc_mark (otb_application->name);
    return SCM_BOOL_F;
  }

  static size_t
  free_otb_application (SCM otb_application_smob)
  {
    struct otb_application *otb_application = (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    scm_gc_free (otb_application, sizeof (struct otb_application), "otb_application");

    return 0;
  }

  static int
  print_otb_application (SCM otb_application_smob, SCM port, scm_print_state *pstate)
  {
    struct otb_application *otb_application = (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    scm_puts ("#<otb_application ", port);
    scm_display (otb_application->name, port);
    scm_puts (">", port);

    scm_remember_upto_here_1(otb_application_smob);
    /* non-zero means success */
    return 1;
  }

  /** ----------------------------------------------------------------------------------- */

  /** Get the keys for the application parameters */
  SCM get_otb_application_parameters (SCM otb_application_smob)
  {

    struct otb_application *otb_application =
      (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);
    std::vector<std::string> applicationParameters =
      otb_application->application->GetParametersKeys(true);

    scm_remember_upto_here_1(otb_application_smob);
    return vec_of_strings_to_list( applicationParameters );
  }

  /** Get the long name for a parameter given its key */
  SCM get_otb_application_parameter_name(SCM otb_application_smob, SCM parameter_key)
  {
    struct otb_application *otb_application =
      (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    scm_remember_upto_here_1(otb_application_smob);
    return scm_from_utf8_string(
      (otb_application->application->GetParameterName(
        std::string(scm_to_utf8_string(parameter_key)))).c_str());
  }

  /** Set a parameter value */
  SCM set_otb_application_parameter_stringlist(SCM otb_application_smob, SCM parameter_key, SCM string_list)
  {
    struct otb_application *otb_application =
      (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    // convert the scheme string list to the type expected by the application
    std::vector<std::string> string_list_value = list_to_vec_of_strings(string_list);
    
    otb_application->application->SetParameterStringList(std::string(scm_to_utf8_string(parameter_key)),
                                                         string_list_value);

    scm_remember_upto_here_1(otb_application_smob);
    return scm_from_unsigned_integer(0);
  }

  SCM set_otb_application_parameter_string(SCM otb_application_smob, SCM parameter_key, SCM str)
  {
    struct otb_application *otb_application =
      (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    // convert the scheme string list to the type expected by the application
    std::string string_value = std::string(scm_to_utf8_string(str));
    
    otb_application->application->SetParameterString(std::string(scm_to_utf8_string(parameter_key)),
                                                         string_value);

    scm_remember_upto_here_1(otb_application_smob);
    return scm_from_unsigned_integer(0);
  }

    SCM set_otb_application_parameter_int(SCM otb_application_smob, SCM parameter_key, SCM val)
  {
    struct otb_application *otb_application =
      (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    // convert the scheme string list to the type expected by the application
    int  int_value = scm_to_int(val);
    
    otb_application->application->SetParameterInt(std::string(scm_to_utf8_string(parameter_key)),
                                                         int_value);

    scm_remember_upto_here_1(otb_application_smob);
    return scm_from_unsigned_integer(0);
  }

  
  SCM set_otb_application_parameter_output_pixel_type(SCM otb_application_smob, SCM str)
  {
    struct otb_application *otb_application =
      (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    // convert the scheme string list to the type expected by the application
    std::string string_value = std::string(scm_to_utf8_string(str));

    otb::Wrapper::ImagePixelType outPixType = otb::Wrapper::ImagePixelType_float;

    if (string_value == "uint8")
      outPixType = otb::Wrapper::ImagePixelType_uint8;
    else if (string_value == "int16")
      outPixType = otb::Wrapper::ImagePixelType_int16;
    else if (string_value == "uint16")
      outPixType = otb::Wrapper::ImagePixelType_uint16;
    else if (string_value == "int32")
      outPixType = otb::Wrapper::ImagePixelType_int32;
    else if (string_value == "uint32")
      outPixType = otb::Wrapper::ImagePixelType_uint32;
    else if (string_value == "float")
      outPixType = otb::Wrapper::ImagePixelType_float;
    else if (string_value == "double")
      outPixType = otb::Wrapper::ImagePixelType_double;
    else
      {
      scm_throw(scm_from_utf8_symbol("otb-application-exception"), scm_from_utf8_symbol("Wrong output parameter type requested!"));
      }

    otb_application->application->SetParameterOutputImagePixelType(std::string("out"),
                                                                   outPixType);

    scm_remember_upto_here_1(otb_application_smob);
    return scm_from_unsigned_integer(0);
  }

  /** Execute the application */
  SCM execute_otb_application(SCM otb_application_smob)
  {
    struct otb_application *otb_application =
      (struct otb_application *) SCM_SMOB_DATA (otb_application_smob);

    otb_application->application->ExecuteAndWriteOutput();

    try
      {
      scm_remember_upto_here_1(otb_application_smob);
      }
    catch (...)
      {
      scm_throw(scm_from_utf8_symbol("otb-application-exception"), scm_from_utf8_symbol(scm_to_utf8_string(otb_application->name)));
      }
    return scm_from_unsigned_integer(0);
  }
  /** Get the list of all the available applications on the path*/
  SCM get_otb_application_list ()
  {
    std::vector<std::string> availableApplications = otb::Wrapper::ApplicationRegistry::GetAvailableApplications();

    return vec_of_strings_to_list( availableApplications );
  }

  /** ----------------------------------------------------------------------------------- */

  /** Expose the application smob and the available functions for guile */  
  void
  init_otb_application (void *unused)
  {
    otb_application_tag = scm_make_smob_type ("otb_application", sizeof (struct otb_application));
    scm_set_smob_mark (otb_application_tag, mark_otb_application);
    scm_set_smob_free (otb_application_tag, free_otb_application);
    scm_set_smob_print (otb_application_tag, print_otb_application);
    
    scm_c_define_gsubr ("clear-otb-application", 1, 0, 0, reinterpret_cast<scm_t_subr>(clear_otb_application));
    scm_c_define_gsubr ("make-otb-application", 1, 0, 0, reinterpret_cast<scm_t_subr>(make_otb_application));
    scm_c_define_gsubr ("get-otb-application-list", 0, 0, 0, reinterpret_cast<scm_t_subr>(get_otb_application_list));
    scm_c_define_gsubr ("get-otb-application-parameters", 1, 0, 0, reinterpret_cast<scm_t_subr>(get_otb_application_parameters));
    scm_c_define_gsubr ("get-otb-application-parameter-name", 2, 0, 0, reinterpret_cast<scm_t_subr>(get_otb_application_parameter_name));
    scm_c_define_gsubr ("set-otb-application-parameter-stringlist", 3, 0, 0, reinterpret_cast<scm_t_subr>(set_otb_application_parameter_stringlist));
    scm_c_define_gsubr ("set-otb-application-parameter-string", 3, 0, 0, reinterpret_cast<scm_t_subr>(set_otb_application_parameter_string));
    scm_c_define_gsubr ("set-otb-application-parameter-int", 3, 0, 0, reinterpret_cast<scm_t_subr>(set_otb_application_parameter_int));
    scm_c_define_gsubr ("set-otb-application-parameter-output-pixel-type", 2, 0, 0, reinterpret_cast<scm_t_subr>(set_otb_application_parameter_output_pixel_type));
    scm_c_define_gsubr ("execute-otb-application", 1, 0, 0, reinterpret_cast<scm_t_subr>(execute_otb_application));
  }

  void
  scm_init_otb_application_module ()
  {
    scm_c_define_module ("otb application", init_otb_application, NULL);
  }


}

