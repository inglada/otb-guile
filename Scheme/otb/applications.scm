;; =========================================================================
;;
;;   Program:   otb-guile
;;
;;   Copyright (c) Jordi Inglada. All rights reserved.
;;
;;   See copyright.txt for details.
;;
;;   This software is distributed WITHOUT ANY WARRANTY; without even
;;   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;;   PURPOSE.  See the above copyright notices for more information.
;;
;; =========================================================================

(define-module (otb applications)
  #:use-module ((otb wrappers))
  #:export (band-math
            quick-look
            extract-roi
            concatenate-images
            compute-image-statistics
            train-classifier
            image-classifier
            validate-classifier))

(define* (band-math input-image-list output-image exp #:optional (pixel-type "float"))
  "Run the OTB BandMath application"
  (let ((bm-app (make-otb-application "BandMath")))
    (begin
      (set-otb-application-parameter-stringlist bm-app "il" input-image-list)
      (set-otb-application-parameter-string bm-app "out" output-image)
      (set-otb-application-parameter-output-pixel-type bm-app pixel-type)
      (set-otb-application-parameter-string bm-app "exp" exp)
      (execute-otb-application bm-app)
      (clear-otb-application bm-app)
      output-image)))

(define* (quick-look input-image-name output-image-name sampling-ratio #:optional (pixel-type "float"))
  "Generate a quicklook of the image with the given sampling ratio"
  (let ((ql-app (make-otb-application "Quicklook")))
    (begin
      (set-otb-application-parameter-string ql-app "in" input-image-name)
      (set-otb-application-parameter-string ql-app "out" output-image-name)
      (set-otb-application-parameter-output-pixel-type ql-app pixel-type)
      (set-otb-application-parameter-int ql-app "sr" sampling-ratio)
      (execute-otb-application ql-app)
      (clear-otb-application ql-app)
      output-image-name)))

(define* (extract-roi input-image-name output-image-name startx starty sizex sizey #:optional (pixel-type "float"))
  "Extract a ROI from the input image with upper left corner (startx, starty) and size (sizex, sizey)."
  (let ((er-app (make-otb-application "ExtractROI")))
    (begin
      (set-otb-application-parameter-string er-app "in" input-image-name)
      (set-otb-application-parameter-string er-app "out" output-image-name)
      (set-otb-application-parameter-int er-app "startx" startx)
      (set-otb-application-parameter-int er-app "starty" starty)
      (set-otb-application-parameter-int er-app "sizex" sizex)
      (set-otb-application-parameter-int er-app "sizey" sizey)
      (execute-otb-application er-app)
      (clear-otb-application er-app)
      output-image-name)))

(define* (concatenate-images image-list output-image-name #:optional
                             (pixel-type "float"))
  "Concatenate a list of images of the same size into a single multi-channel
   one."
  (let ((ci-app (make-otb-application "ConcatenateImages")))
    (begin
      (set-otb-application-parameter-stringlist ci-app "il" image-list)
      (set-otb-application-parameter-string ci-app "out" output-image-name)
      (set-otb-application-parameter-output-pixel-type ci-app pixel-type)
      (execute-otb-application ci-app)
      (clear-otb-application ci-app)
      output-image-name)))

(define (compute-image-statistics input-image-list output-file)
  "OTB ComputeImagesStatistics application"
  (let ((cis-app (make-otb-application "ComputeImagesStatistics")))
    (begin
      (set-otb-application-parameter-stringlist cis-app "il" input-image-list)
      (set-otb-application-parameter-string cis-app "out" output-file)
      (execute-otb-application cis-app)
      (clear-otb-application cis-app)
      output-file)))

(define (train-classifier input-image-list vector-data-list output-model)
  "Train an image classifier"
  (let ((tic-app (make-otb-application "TrainImagesClassifier")))
    (begin
      (set-otb-application-parameter-stringlist tic-app "io.il" input-image-list)
      (set-otb-application-parameter-stringlist tic-app "io.vd" vector-data-list)
      (set-otb-application-parameter-string tic-app "io.out" output-model)
      (set-otb-application-parameter-string tic-app "sample.vfn" "class")
      (execute-otb-application tic-app)
      (clear-otb-application tic-app)
      output-model)))

(define (image-classifier input-image model validity-mask output-image)
  "Classify the image using the model and the validity mask"
  (let ((ic-app (make-otb-application "ImageClassifier")))
    (begin
      (set-otb-application-parameter-string ic-app "in" input-image)
      (set-otb-application-parameter-string ic-app "model" model)
            (set-otb-application-parameter-string ic-app "mask" validity-mask)
      (set-otb-application-parameter-string ic-app "out" output-image)
      (execute-otb-application ic-app)
      (clear-otb-application ic-app)
      output-image)))

(define (validate-classifier input-image-list input-validation-data-list input-model output-validation-file)
  "Validate the classifier represented by the model using pairs of
images and validation vector data. Generates a file with the
validation statistics."
  (let ((vicapp (make-otb-application "ValidateImagesClassifier")))
    (begin
      (set-otb-application-parameter-stringlist vicapp "il" input-image-list)
      (set-otb-application-parameter-stringlist vicapp "vd" input-validation-data-list)
      (set-otb-application-parameter-string vicapp "model" input-model)
      (set-otb-application-parameter-string vicapp "out" output-validation-file)
      (set-otb-application-parameter-string vicapp "vfn" "class")
      (execute-otb-application vicapp)
      (clear-otb-application vicapp)
      output-validation-file)))
