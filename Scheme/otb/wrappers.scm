;; =========================================================================
;;
;;   Program:   otb-guile
;;
;;   Copyright (c) Jordi Inglada. All rights reserved.
;;
;;   See copyright.txt for details.
;;
;;   This software is distributed WITHOUT ANY WARRANTY; without even
;;   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;;   PURPOSE.  See the above copyright notices for more information.
;;
;; =========================================================================


(define-module (otb wrappers)
  #:export (make-otb-application
            clear-otb-application
            get-otb-application-list
            get-otb-application-parameters
            get-otb-application-parameter-name
            set-otb-application-parameter-stringlist
            set-otb-application-parameter-string
            set-otb-application-parameter-string
            set-otb-application-parameter-int
            set-otb-application-parameter-output-pixel-type
            execute-otb-application))
(load-extension "libOTBApplicationEngineSCM" "init_otb_application")

