;; =========================================================================
;;
;;   Program:   otb-guile
;;
;;   Copyright (c) Jordi Inglada. All rights reserved.
;;
;;   See copyright.txt for details.
;;
;;   This software is distributed WITHOUT ANY WARRANTY; without even
;;   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;;   PURPOSE.  See the above copyright notices for more information.
;;
;; =========================================================================


(define-module (otb cli-application)
  #:export (make-otb-cli-application
            ; TODO get-otb-cli-application-list
            ; TODO get-otb-cli-application-parameters
            get-otb-cli-application-parameter-name
            set-otb-cli-application-parameter-stringlist
            set-otb-cli-application-parameter-string
            set-otb-cli-application-parameter-string
            set-otb-cli-application-parameter-int
            set-otb-cli-application-parameter-output-pixel-type
            execute-otb-cli-application))


(define *default-otb-binary-dir* "/home/inglada/OTB/builds/OTB/bin")
(define *default-otb-module-path* *default-otb-binary-dir*)

(define (make-otb-cli-application app-name)
  "Creates an otb-cli-application"
  (acons "name" app-name '()))

(define (get-otb-cli-application-parameter-name application)
  (assoc-ref application "name"))

(define (parameter-alist-without-name application)
  "Returns a new alist with all the parameters but without the name"
  (assoc-remove! (acons "dummy" 0 application) "dummy"))

(define (parameter-list->string application)
  "Transform an association list holding the parameters into a string with
   dashes in front of each parameter name"
  (map (lambda (parameter) (string-append " -" (car parameter)
                                          " " (cdr parameter)))
       (parameter-alist-without-name application)))

(define* (build-command-line application
                             #:optional (otb-binary-dir *default-otb-binary-dir*)
                              (otb-module-path *default-otb-module-path*))
  (let* ((parameters-string (parameter-list->string application))
         (app-name (get-otb-cli-application-parameter-name application)))
    (string-append otb-binary-dir "/otbcli_"
                   app-name " "
                   otb-module-path " "
                   parameters-string)))

