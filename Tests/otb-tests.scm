#!/usr/bin/guile -s
!#
;; =========================================================================
;;
;;   Program:   otb-guile
;;
;;   Copyright (c) Jordi Inglada. All rights reserved.
;;
;;   See copyright.txt for details.
;;
;;   This software is distributed WITHOUT ANY WARRANTY; without even
;;   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;;   PURPOSE.  See the above copyright notices for more information.
;;
;; =========================================================================

(load-extension "../Wrappers/libOTBApplicationEngineSCM" "init_otb_application") 
(display (get-otb-application-list))
(newline)
(define band-math-1 (make-otb-application "BandMath"))
(define my-app (make-otb-application (car (get-otb-application-list))))
(display (get-otb-application-parameters band-math-1))
(newline)
(display (get-otb-application-parameter-name band-math-1 "il"))
(newline)
;; Get the description of all the parameters for a given application
(display ((λ (ap)
            (map
             (λ (pk)
               (get-otb-application-parameter-name ap pk))
             (get-otb-application-parameters ap)))
          band-math-1))
(newline)
;; Set the value of a parameter
(set-otb-application-parameter-stringlist band-math-1 "il" '("toto.tif" "tit.tif"))
